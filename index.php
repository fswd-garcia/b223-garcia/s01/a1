<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S01: Activity</title>
</head>
<body>
	<p>getFullAddress: <?php echo getFullAddress('Philippines', 'Manila', 'Rizal', 'No 8 Mendiola St.'); ?></p>

	<p>getLetterGrade: <?php echo getLetterGrade(98); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(95); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(92); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(89); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(88); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(85); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(80); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(77); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(75); ?></p>
	<p>getLetterGrade: <?php echo getLetterGrade(74); ?></p>
	
	
</body>
</html>