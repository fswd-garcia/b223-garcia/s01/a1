<?php

/*

S01 - PHP Basics and Selection Control Structures
Activity Instructions: 

1. Create code.php and index.php files inside the a1 folder.
2. In code.php, create a function named getFullAddress() that will take four arguments:
 - Country
 - City
 - Province
 - Specific Address (such as block, lot, or building name and room number).
3. Have this function return the concatenated arguments to result in a coherent address.

*/

function getFullAddress($country, $city, $province, $specificAddress){
	return "$specificAddress, $city, $province, $country";
}


/*
4. Create another function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:
 - A+ (98 to 100)
 - A (95 to 97)
 - A- (92 to 94)
 - B+ (89 to 91)
 - B (86 to 88)
 - B- (83 to 85)
 - C+ (80 to 82)
 - C (77 to 79)
 - C- (75 to 76)
 - D (75 below)
5. Include the code.php in the index.html and invoke the created methods.

*/
function getLetterGrade($str){
	if ($str >= 98){
		return 'A+';
	} else if ($str >= 95){
		return 'A';
	} else if ($str >= 95){
		return 'A';
	} else if ($str >= 92){
		return 'A-';
	} else if ($str >= 89){
		return 'B+';
	} else if ($str >= 86){
		return 'B';
	} else if ($str >= 83){
		return 'B-';
	} else if ($str >= 80){
		return 'C+';
	} else if ($str >= 77){
		return 'C';
	} else if ($str >= 75){
		return 'C-';
	} else {
		return 'D';
	}
	



}